import {
	LightningElement,
	api,
	wire,
	track
} from "lwc";
import getUserDetailsInfo from "@salesforce/apex/apexClass.getuserDetails";
const recordsPerPage = [50, 100, 200];

const columns = [
    {
		label: "Select User",
		hideDefaultActions: "true",
		initialWidth: 100
	},
    {
		label: "Agent",
		fieldName: "name",
		sortable: true,
		hideDefaultActions: "true",
		initialWidth: 100
	}
];

export default class NmCaseMonitoringAssignAgents extends LightningElement {
	
	connectedCallback() {
		this.getUserDetails();
	}


    

	

	@track sortBy = 'patientId';
	@track sortOrder = 'asc';
	
	handleSort(event){ 
		
		let fieldName = event.currentTarget.dataset.fieldname;
		let isValid = false;
		this.columns.forEach(column => {
            //check if column is sortable
            if (!column.sortable) {
                return;
            }

            //Find the correct column and update the values for others as well
            if (column.fieldName === fieldName) {
                isValid = true;
                column.sorted = true;
                column.sortedAscending = !column.sortedAscending;

                this.sortBy = fieldName;
                this.sortOrder = column.sortedAscending ? 'asc' : 'desc';
            } else {
                column.sorted = false;
            }
        });

        if (!isValid) {
            return;
		}
		
		this.updateTable();

		event.stopPropagation();
	}

	updateTable(){ 
		let data = this.data;
		data = data.sort(this.dynamicsort(this.sortBy, this.sortOrder));
		this.data = data;
		console.log('Data sorted: ', JSON.parse(JSON.stringify(this.data)));
    }
    
	

	dynamicsort(property, order) {
       
		
		return function (a, b) {
			if (a[property] === b[property]){ 
				return 0;
			}
			else if(!a[property]){ 
				return 1;
			}
			else if(!b[property]){ 
				return -1;
			}
			else if(order === "asc"){ 
				return a[property] < b[property] ? -1 : 1;
			}
			else { 
				return a[property] < b[property] ? 1 : -1;;
			}
        }
	}

	

	
	@track data = [];
	@track originalData = [];
	columns = columns;
    @track showSpinner = false;




	getUserDetails() {
		this.showSpinner = true;
        getUserDetailsInfo({
            })
            .then((wrapperResult) => {

				this.data = wrapperResult.usersList;
                this.originalData = this.data;
               
				this.showSpinner = false;
				this.filterRecords();
            })
            .catch((error) => {
                this.showSpinner = false;
                let erroMsg = error.message || error.body.message;
                this.dispatchEvent(showToast("", erroMsg, "error"));
            });
            
	}

}